docker container stop $(docker container ls -aq)
docker container rm $(docker container ls -aq)
docker system prune -f
docker build -t mangacatalogue:latest -f ./Dockerfile ./
docker run -p 3000:3000 mangacatalogue:latest