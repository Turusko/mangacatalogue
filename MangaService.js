const express = require('express');
const app = express();
const port = 3000;
var bodyParser = require('body-parser');
const fs = require('fs');
var jsonParser = bodyParser.json();

app.get('/', (req, res) => {
  res.send('Hello World! 2');
});

app.get('/api/v1/get_manga_list', (req, res) => {
  console.log("Im hit")
  try {
    var files = fs.readdirSync('Manga');
    files.forEach(function(element, index, array) {
      array[index] = element.replace(".json", "");
    });
    res.send(files);
  } catch (ENOENT) {
    res.send("No Manga Saved");
  };
    
  });

app.post('/api/v1/add_manga', jsonParser, (req, res) => {
    let filename = req.body.mangaTitle + '.json';
    SaveJsonFile(filename, req.body);
    res.send(req.body);

  });

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});


function SaveJsonFile(filename, jsonData)
{
  
  fs.writeFile(`manga\\${filename}`, JSON.stringify(jsonData), 'utf8', function(err) {
    if (err) {
        console.log(err);
    }});
}