const { text } = require('body-parser');
const { response } = require('express');
const express = require('express');
const app = express();
const port = 3001;
const mangaService = 'http://localhost:3000'
const getMangaListApi = '/api/v1/get_manga_list'


app.get('/', (req, res) => {
    
    var text = JSON.parse(httpGet(mangaService+getMangaListApi))
    res.send(text);
  });



app.listen(port, () => {
console.log(`Example app listening at http://localhost:${port}`);
});


function httpGet(url)
{

    let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    let xhr = new XMLHttpRequest();
 
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            return xhr.responseText;
        }
    }

    xhr.open('GET',url, false);
    xhr.send();
    return xhr.onreadystatechange();
}